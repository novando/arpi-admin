const customTheme = require('./src/styles/variables/index.cjs')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,vue}",
  ],
  theme: {
    extend: customTheme,
  },
  plugins: [],
}
