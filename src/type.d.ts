declare interface variantDataType {
  id: number,
  name: string,
  max_capacity: number,
  duration: number,
  price: number,
  days: string,
}
declare interface paxType {
  name: string,
  sex: string,
  age: number,
  province: string,
  city: string,
}
declare interface ticketDataType {
  id: number,
  id_ticket: string,
  price: number,
  phone: string,
  pic: string,
  venue_id: number,
  ticket_status_id: number,
  package_name?: string,
  variant_name?: string,
  pax: paxType[],
  created_at: string,
}
declare interface venueDataType {
  id: number,
  owner: string,
  name: string,
  capacity: number,
}
declare interface manifestDataType {
  departure: string,
  id_ticket: string,
  pax_name: string,
  pax_sex: string,
  pax_age: number,
  pax_address: string,
  pax_phone: string,
  ticket: string,
  pic: string,
  price?: number | string,
}
declare interface manifestRecapDataType {
  name: string,
  price?: number,
  package?: string,
  venue?: string,
  total_ticket: number,
  total_pax: number,
  total_sale: number,
}
declare interface manifestRecapInstallmentDataType {
  name: string,
  total_ticket: number,
  total_pax: number,
  total_sale: number,
  total_ticket_premium: number,
  total_pax_premium: number,
}
declare interface companyDataType {
  id: number,
  name: string,
  owner: string,
  secretary: string,
  finance: string,
  phone: string,
  province: string,
  city: string,
}
declare interface paxRecapDataType {
  name: string,
  price: number,
  total_pax: number,
  total_sale: number,
  daily: number[],
}
declare interface paxRecapPremiumDataType {
  name: string,
  price: number,
  tariff: number,
  total_pax: number,
  total_sale: number,
  total_tariff: number,
  daily: number[],
}
interface packageDataType {
  id: number,
  name: string,
  description: string,
  price: number,
}
interface segmentDataType {
  baby: number,
  child: number,
  teen: number,
  young: number,
  adult: number,
  senior: number,
  total: number,
}
interface genderDataType {
  male: number,
  female: number,
  total: number,
}
interface provinceDataType {
  id: string,
  name: string,
}
interface cityDataType {
  id: string,
  province_id: string,
  name: string,
}