const colors = require('./colors.cjs')
const spacing = require('./spacing.cjs')

module.exports = {
  colors: colors,
  spacing: spacing,
}