import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/package`

export default {
  getPackages () {
    return fetchApi.getLogged(`${apiSite}`)
  },
  addPackage (payload:packageType) {
    return fetchApi.postLogged(`${apiSite}`, payload)
  },
  getPackageDetail (id:string) {
    return fetchApi.getLogged(`${apiSite}/${id}`)
  },
  delPackage (id:string) {
    return fetchApi.delLogged(`${apiSite}/${id}`)
  },
  editPackage (id:string, payload:packageType) {
    return fetchApi.editLogged(`${apiSite}/${id}`, payload)
  },
}