import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/download`

export default {
  getManifest (venueId:string, query:getManifestQuery = {}, filename:string) {
    return fetchApi.dlLogged(`${apiSite}/manifest/${venueId}`, query, filename)
  },
  getManifestRecap (by:string ,companyId:string, query:getManifestQuery = {}, filename:string) {
    return fetchApi.dlLogged(`${apiSite}/manifest-recap/${by}/${companyId}`, query, filename)
  },
  getManifestRecapInstallment (companyId:string, query:getManifestQuery = {}, filename:string) {
    return fetchApi.dlLogged(`${apiSite}/manifest-installment/${companyId}`, query, filename)
  },
  getPaxRecap (companyId:string, query:getManifestQuery = {}, filename:string) {
    return fetchApi.dlLogged(`${apiSite}/ticket-recap/${companyId}`, query, filename)
  },
  getPaxRecapPremium (companyId:string, query:getManifestQuery = {}, filename:string) {
    return fetchApi.dlLogged(`${apiSite}/ticket-premium-recap/${companyId}`, query, filename)
  },
}