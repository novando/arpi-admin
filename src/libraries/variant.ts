import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/variant`

export default {
  getVariants () {
    return fetchApi.getLogged(`${apiSite}`)
  },
  getPackage (id:string) {
    return fetchApi.getLogged(`${apiSite}/package/${id}`)
  },
  addVariant (payload:packageType) {
    return fetchApi.postLogged(`${apiSite}`, payload)
  },
  getVariantDetail (id:string) {
    return fetchApi.getLogged(`${apiSite}/${id}`)
  },
  delVariant (id:string) {
    return fetchApi.delLogged(`${apiSite}/${id}`)
  },
  editVariant (id:string, payload:packageType[]) {
    return fetchApi.editLogged(`${apiSite}/${id}`, payload)
  },
}