import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/ticket`

export default {
  getTickets () {
    return fetchApi.getLogged(`${apiSite}`)
  },
  addTicket (payload:any) {
    return fetchApi.postLogged(`${import.meta.env.VITE_API_SITE}/public/pax/add`, payload)
  },
  getTicketDetail (id:string) {
    return fetchApi.getLogged(`${apiSite}/details/${id}`)
  },
  getUnfinishedOrder (companyId = localStorage.getItem('companyId')) {
    return fetchApi.getLogged(`${apiSite}/unfinished-order/${companyId}`)
  },
  editTicket (id:string, payload:any) {
    return fetchApi.editLogged(`${apiSite}/${id}`, payload)
  },
}