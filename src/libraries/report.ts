import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/report`

export default {
  getManifest (venueId:string, query:getManifestQuery = {}) {
    return fetchApi.getLogged(`${apiSite}/manifest/${venueId}`, query)
  },
  getManifestRecap (by:string ,companyId:string, query:getManifestQuery = {}) {
    return fetchApi.getLogged(`${apiSite}/manifest-recap/${by}/${companyId}`, query)
  },
  getManifestRecapInstallment (companyId:string, query:getManifestQuery = {}) {
    return fetchApi.getLogged(`${apiSite}/manifest-installment/${companyId}`, query)
  },
  getPaxRecap (companyId:string, query:getManifestQuery = {}) {
    return fetchApi.getLogged(`${apiSite}/ticket-recap/${companyId}`, query)
  },
  getPaxRecapPremium (companyId:string, query:getManifestQuery = {}) {
    return fetchApi.getLogged(`${apiSite}/ticket-premium-recap/${companyId}`, query)
  },
}