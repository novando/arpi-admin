declare type loginType = {
  email: string,
  password: string,
}
declare type registerType = {
  name: string,
  email: string,
  password: string,
  role_id?: number,
  company_id?: number,
}
declare type companyType = {
  name: string,
  logo: string,
  owner: string,
  secretary: string,
  finance: string,
  province: string,
  city: string,
  address: string,
  phone: string,
}
declare type packageType = {
  name: string,
  price: number,
  duration: number,
}
declare type variantType = {
  id: number,
  name: string,
  days: string,
}
declare type getManifestQuery = {
  startDate?: string,
  endDate?: string,
}