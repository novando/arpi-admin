import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/analytics`
const defaultCompanyId = localStorage.getItem('companyId')

export default {
  getPax (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/pax/${companyId}`, query)
  },
  getPackage (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/package/${companyId}`, query)
  },
  getVenue (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/venue/${companyId}`, query)
  },
  getSale (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/sale/${companyId}`, query)
  },
  getSegment (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/pax-segment/${companyId}`, query)
  },
  getHometown (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/pax-hometown/${companyId}`, query)
  },
  getGender (query:getManifestQuery = {}, companyId = defaultCompanyId) {
    return fetchApi.getLogged(`${apiSite}/pax-gender/${companyId}`, query)
  },
}