import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/company`

export default {
  getCompanies () {
    return fetchApi.getLogged(`${apiSite}`)
  },
  getMine () {
    return fetchApi.getLogged(`${apiSite}/mine`)
  },
  addCompany (payload:companyType) {
    return fetchApi.postLogged(`${apiSite}`, payload)
  },
  getCompanyDetail (id:string) {
    return fetchApi.getLogged(`${apiSite}/one/${id}`)
  },
  delCompany (id:string) {
    return fetchApi.delLogged(`${apiSite}/one/${id}`)
  },
  editCompany (payload:companyType) {
    return fetchApi.editLogged(`${apiSite}`, payload)
  },
}