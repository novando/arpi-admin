import fetchApi from '@/factories/fetch'

const apiSite = `${import.meta.env.VITE_API_SITE}/venue`

export default {
  getVenues () {
    return fetchApi.getLogged(`${apiSite}`)
  },
  getVenuesCompany (companyId:string) {
    return fetchApi.getLogged(`${apiSite}/company/${companyId}`)
  },
  getVenueDetail (id:string) {
    return fetchApi.getLogged(`${apiSite}/detail/${id}`)
  },
  addVenue (payload:packageType) {
    return fetchApi.postLogged(`${apiSite}`, payload)
  },
  editVenue (payload:packageType, id:string) {
    return fetchApi.editLogged(`${apiSite}/${id}`, payload)
  },
  delVenue (id:string) {
    return fetchApi.delLogged(`${apiSite}/${id}`)
  },
}