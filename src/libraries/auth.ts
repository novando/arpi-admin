import cookie from '@/factories/cookie'
import fetchApi from '@/factories/fetch'

const authUrl = `${import.meta.env.VITE_API_SITE}/auth`

export default {
  login (payload:loginType) {
    return fetchApi.postData(`${authUrl}/login`, payload)
      .then((res:any) => {
        cookie.setCookie('accessToken', res, 30)
      })
      .catch((err:any) => {
        throw err
      })
  },
  logout () {
    return cookie.delCookie('accessToken')
  },
  register (payload:registerType) {
    return fetchApi.postData(`${authUrl}/register`, payload)
  },
  me () {
    return fetchApi.getLogged(`${authUrl}/me`)
      .then((res:number) => {
        return res
      })
      .catch((err:any) => {
        cookie.delCookie('accessToken')
        return Promise.reject(err)
      })
  },
  profile () {
    return fetchApi.getLogged(`${authUrl}/profile`)
      .then((res:any) => {
        localStorage.setItem('id', res.id)
        localStorage.setItem('name', res.name)
        localStorage.setItem('email', res.email)
        localStorage.setItem('companyId', res.company_id)
        localStorage.setItem('roleId', res.role_id)
        return
      })
      .catch((err:any) => {
        return Promise.reject(err)
      })
  },
}