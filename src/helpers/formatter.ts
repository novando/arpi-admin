const timezone = '+07:00'

// const timestampToDate = (val:string) => {
//   const timeStr = (val.split('T')[1]).split('.')[0]
//   const dateStr = (val.split('T')[0])
//   const date = new Date(`${dateStr} ${timeStr}`)
//   date.setHours(date.getHours() + timezone)
//   return date
// }

export default {
  currency (val:number) {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(val)
  },
  numberPretty (val:number) {
    return new Intl.NumberFormat('id-ID').format(val)
  },
  toDate (val:string) {
    if (val === '') return
    const date = new Date(val)
    const months = [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    ]
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
  },
  toDateShort (val:string) {
    if (val === '') return
    const date = new Date(val)
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
  },
  toDateIso (val:string) {
    if (val === '') return
    const date = new Date(val)
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
  },
  toTime (val:string) {
    if (val === '') return
    const date = new Date(val)
    const hour = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()
    const minute = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
    return `${hour}:${minute}`
  },
  toTimeIso (val:string) {
    if (val === '') return
    const date = new Date(val + timezone)
    const Y = date.getUTCFullYear()
    const M = date.getUTCMonth() + 1 < 10 ? `0${date.getUTCMonth() + 1}` : date.getUTCMonth() + 1
    const D = date.getUTCDate() < 10 ? `0${date.getUTCDate()}` : date.getUTCDate()
    const h = date.getUTCHours()
    const m = date.getUTCMinutes()
    const s = date.getUTCSeconds()
    return `${Y}-${M}-${D}T${h}:${m}:${s}`
  },
  twoDigit (val:number) {
    if (val > 99) return
    return val < 10 ? `0${val}` : val
  }
}