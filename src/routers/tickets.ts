import routeGuards from '@/factories/route-guards'

const Ticket = () => import('../pages/tickets/Ticket.vue')
const NewOrder = () => import('../pages/tickets/NewOrder.vue')

export default [
  {
    path: '/ticket/',
    name: 'ticket',
    component: Ticket,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Tiket',
    },
  },
  {
    path: '/ticket/new-order/:id',
    name: 'ticket-new-order',
    meta: {
      displayName: 'Detail Tiket',
      routeHistory: [
        'Ticket',
        'New Order',
      ]
    },
    component: NewOrder,
    beforeEnter: routeGuards.isLogin,
  }
]