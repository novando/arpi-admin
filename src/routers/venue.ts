import routeGuards from '@/factories/route-guards'

const Venue = () => import('../pages/venues/Venue.vue')

export default [
  {
    path: '/venue',
    name: 'venue',
    component: Venue,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Venue',
    },
  },
]