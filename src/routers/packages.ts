import routeGuards from '@/factories/route-guards'

const Package = () => import('../pages/packages/Package.vue')
const PackageDetails = () => import('../pages/packages/PackageDetails.vue')

export default [
  {
    path: '/package',
    name: 'package',
    component: Package,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Paket',
    },
  },
  {
    path: '/package/add-package',
    name: 'add-package',
    component: PackageDetails,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Paket Baru',
    },
  },
  {
    path: '/package/edit-package/:id',
    name: 'edit-package',
    component: PackageDetails,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Detail Paket',
    },
  },
]