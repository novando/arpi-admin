import { createRouter, createWebHistory } from 'vue-router'
import dashboards from './dashboards'
import essentials from './essentials'
import tickets from './tickets'
import packages from './packages'
import venues from './venue'
import settings from './settings'
import reports from './reports'
import registers from './registers'
import cashiers from './cashiers'

interface vueRouteType {
  path: string,
  component: any,
  name: string | null,
  params?: any,
  meta?: any,
  beforeEnter?: any,
}

const routes:vueRouteType[] = [
  ...dashboards,
  ...tickets,
  ...packages,
  ...venues,
  ...settings,
  ...reports,
  ...registers,
  ...cashiers,
  ...essentials, // Should always last on the array list
]

export default createRouter({
  history: createWebHistory(),
  // @ts-ignore
  routes: routes,
})
