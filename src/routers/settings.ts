import routeGuards from '@/factories/route-guards'

const Settings = () => import('@/pages/Settings.vue')

export default [
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Pengaturan',
    },
  },
]