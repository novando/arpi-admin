import routeGuards from '@/factories/route-guards'

const Register = () => import('../pages/register/Register.vue')
const Account = () => import('../pages/register/Account.vue')
const Company = () => import('../pages/register/Company.vue')

export default [
  {
    path: '/register',
    name: 'register',
    component: Register,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Pendaftaran',
    },
  },
  {
    path: '/register/company',
    name: 'register-company',
    component: Company,
    meta: {
      displayName: 'Pendaftaran Perusahaan Baru',
    },
  },
  {
    path: '/register/account',
    name: 'register-account',
    component: Account,
    meta: {
      displayName: 'Pendaftaran Akun Baru',
    },
  },
]