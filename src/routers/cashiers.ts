import routeGuards from '@/factories/route-guards'

const Cashier = () => import('../pages/Cashier.vue')

export default [
  {
    path: '/cashier',
    name: 'cashier',
    component: Cashier,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Kasir',
    },
  },
]