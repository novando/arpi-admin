import routeGuards from '@/factories/route-guards'

const Dashboard = () => import('../pages/dashboards/Dashboard.vue')

export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Dashboard',
    },
  },
]