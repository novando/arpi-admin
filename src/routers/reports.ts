import routeGuards from '@/factories/route-guards'

const RecapManifest = () => import('@/pages/reports/ManifestRecap.vue')
const RecapManifestInstallment = () => import('@/pages/reports/ManifestRecapInstallment.vue')
const RecapPax = () => import('@/pages/reports/PaxRecap.vue')
const RecapPaxPremium = () => import('@/pages/reports/PaxRecapPremium.vue')
const Manifest = () => import('@/pages/reports/Manifest.vue')

export default [
  {
    path: '/manifest',
    name: 'manifest',
    component: Manifest,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Manifes',
    },
  },
  {
    path: '/recap-manifest',
    name: 'recap-manifest',
    component: RecapManifest,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Rekap Penjualan',
    },
  },
  {
    path: '/recap-installment',
    name: 'recap-installment',
    component: RecapManifestInstallment,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Rekap Premi Penjualan',
    },
  },
  {
    path: '/recap-pax',
    name: 'recap-passenger',
    component: RecapPax,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Rekap Penumpang',
    },
  },
  {
    path: '/recap-premium-pax',
    name: 'recap-premium-passenger',
    component: RecapPaxPremium,
    beforeEnter: routeGuards.isLogin,
    meta: {
      displayName: 'Rekap Premi Penumpang',
    },
  },
]