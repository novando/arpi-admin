const Auth = () => import('../pages/Auth.vue')
const Home = () => import('../pages/Home.vue')

export default [
  {
    path: '/login',
    name: 'login',
    component: Auth,
  },
  // The following block should be on the last order
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: () => {
      const roleId = parseInt(localStorage.getItem('roleId') || '99')
      if (roleId > 3) return '/ticket'
      return '/dashboard'
    },
    // beforeEnter: (to:any, from:any) => {
    //   return '/dashboard'
    // }
  },
]