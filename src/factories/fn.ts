let throttleTimer:boolean

export default {
  throttle (callback:any, time:number) {
    if (throttleTimer) return
    throttleTimer = true
    setTimeout(() => {
      callback()
      throttleTimer = false
    }, time)
  }
}