import auth from '@/libraries/auth'

const appUrl = import.meta.env.VITE_APP_SITE

export default {
  isLogin (to:any, from:any) {
    return auth.me()
      .then(() => {
        if (from.path === '/login' && from.query.redirect !== undefined) {
          console.log(window.location)
          window.location.href = appUrl + from.query.redirect
        }
        return
      })
      .catch((err:any) => {
        console.log(err)
        return {path: '/login', query: {redirect: to.path}}
      })
  } 
}