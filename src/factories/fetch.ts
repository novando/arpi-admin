import cookie from "./cookie"

export default {
  getData (url:string) {
    return fetch(url, {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
      }
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  postData (url:string, payload:any) {
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-Type': 'application/json'
      },
      body: JSON.stringify(payload),
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  getLogged (url:string, query:any = {}) {
    const token = cookie.getCookie('accessToken') as string
    const queryParam = new URLSearchParams(query)
    url += `?${queryParam.toString()}` 
    cookie.setCookie('accessToken', token, 30)
    return fetch(url, {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        } else {
          return res.json()
        }
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  postLogged (url:string, payload:any) {
    const token = cookie.getCookie('accessToken') as string
    cookie.setCookie('accessToken', token, 30)
    return fetch(url, {
      method: 'POST',
      headers: {
        'content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(payload),
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  editLogged (url:string, payload:any) {
    const token = cookie.getCookie('accessToken') as string
    cookie.setCookie('accessToken', token, 30)
    return fetch(url, {
      method: 'PUT',
      headers: {
        'content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(payload),
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  delLogged (url:string) {
    const token = cookie.getCookie('accessToken') as string
    cookie.setCookie('accessToken', token, 30)
    return fetch(url, {
      method: 'DELETE',
      headers: {
        'content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        }
        return res.json()
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        throw err
      })
  },
  dlLogged (url:string, query:any = {}, filename:string) {
    const token = cookie.getCookie('accessToken') as string
    const queryParam = new URLSearchParams(query)
    url += `?${queryParam.toString()}` 
    cookie.setCookie('accessToken', token, 30)
    return fetch(url, {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
      .then((res) => {
        if (res.status < 200 && res.status >= 400) {
          throw res.statusText
        } else {
          return res.blob()
        }
      })
      .then((res) => {
        const a = document.createElement('a')
        a.href = window.URL.createObjectURL(res)
        a.download = filename
        a.click()
      })
      .catch((err) => {
        throw err
      })
  },
}