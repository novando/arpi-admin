export default {
  'essentials': [
    './src/pages/Auth.vue',
    './src/pages/Home.vue',
  ],
  'dashboards': [
    './src/pages/dashboards/Dashboard.vue',
    './src/pages/Settings.vue',
  ],
  'manifests': [
    './src/pages/reports/Manifest.vue',
    './src/pages/reports/ManifestRecap.vue',
  ],
  'packages': [
    './src/pages/packages/PackageDetails.vue',
    './src/pages/packages/Package.vue',
  ],
  'tickets': [
    './src/pages/tickets/Ticket.vue',
    './src/pages/tickets/NewOrder.vue',
  ],
  'venues': [
    './src/pages/venues/Venue.vue',
  ],
}